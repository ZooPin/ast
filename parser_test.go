package regularInterpreter

import "testing"

func TestParser_Read(t *testing.T) {
	values := []string {
		"(ab*)|(a*b)",
	}

	parser := Parser{}
	for _, input := range values {
		parser.New()
		parser.Read(input)
	}
}