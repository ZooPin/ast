package regularInterpreter

import "errors"

const (
	comp = iota
	cons = iota
)

type composite struct {
	_type int
	value string
	repeatable bool
	child []*composite
}

func (c *composite) AddChild(child *composite) error {
	if c._type != comp {
		return errors.New("composite is type constant and not a composite")
	}
	c.child = append(c.child, child)

	return nil
}