package regularInterpreter

type Parser struct {
	ast []*composite
}

func (Parser) New() Parser {
	return Parser{}
}

func (p *Parser) Read(input string) error {
	for i := 0; i < len(input); i++ {
		if input[i] == '(' && input[i+1] != ')' {
			box := &composite{
				comp,
				"AND",
				false,
				[]*composite{},
			}
			n := i+1
			for input[n] != ')'{

				constant := &composite{
					cons,
					string(input[n]),
					false,
					nil,
				}

				if input[n+1] == '*' {
					n++
					constant.repeatable = true
				}

				box.AddChild(constant)
				n++
			}
			i = n
			if len(input)+1 < n+1 && input[n+1] == '|' {
				n++
				box.value = "OR"
			}
			p.ast = append(p.ast, box)
		}
	}
	return nil
}